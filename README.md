# The Democracy Box

The Democracy Box makes it easy conduct secure and transparent elections in your
community, organization, or group.

## Summary

We're making it easy to run next-generation democratic organizations. From
simple majority executive elections to ranked-approval voting for referendum
options; from paper ballots to multi-modal voting back by blockchain
record-keeping; from your community school-board to international distributed
collectives: the Democracy Box is a suite of open source tools to effortlessly
integrate the most state-of-the-art democratic processes into the groups you
belong to.

  * Use the Democracy Box open source hardware to print, record, and tally paper
votes using only a standard off-the-shelf all-in-one printer/scanner.
  * Use the DemocracyBox.online portal to manage more complex ballots and voter
rolls, integrate with CRM, blockchain, and offline voter role systems, and
receive ballots online.
  * Use the Democracy Box app to manage your Democracy Box appliance, vote via
mobile device, and get realtime results.

## Status

Currently the Democracy Box is an emerging concept. To get involved, contact
ethan@colab.coop.
